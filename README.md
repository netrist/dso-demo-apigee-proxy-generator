# DSO Demo Apigee Proxy Generator

This tool automates proxy creation based on an OAS 3.0 API spec. It also adds policies to the API prior to deploying it to Apigee.

### Pre-requisites ###

This tool relies on several excellent free tools maintained by Apigee and others. With NodeJS and Node Package Manager (NPM) installed on your system, add these tools:

```
npm install -g xslt3  #also installs saxon-js
npm install -g apigeetool
```

You will also need an Apigee account. You can evaluate Apigee for free by signing-up online.

### Variables ###

You will need to set values for the following variables. Set the values at the beginning of the createAndDeployProxy.sh. You will need:

PROXY_NAME - the name of the proxy, you will see a folder with this name locally and this is the name of the proxy in apigee

OAS_DEFINITION - path and filename of the OAS 3.0 definition (JSON or YAML format)

BASE_PATH - the relative path after the Apigee URL

APIGEE_ENV - Which Apigee environment to deploy to

APIGEE_USER - Apigee username

APIGEE_PASSWORD - Apigee password

APIGEE_ORG - Apigee organization linked to your account

### Running the Tool ###

Once the variables are set, execute the script:

`./createAndDeployProxy.sh`

### Modify Global Policies ###

In this version of the tool global policies are "static" and sequenced according to best practice. You can change the thresholds for the Spike Arrest and Quota policies by editing the XML found in the global-policies folder. 

Future versions of this tool may provide configuration control over thresholds or a graphical user interface for selecting proxy options.