<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:strip-space elements="*"/>
    <xsl:param name="GLOBAL-REQUEST-POLICY-NAMES"/>
    <xsl:param name="BASE_PATH"/>
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/ProxyEndpoint/PreFlow/Request">
        <xsl:element name="Request">
            <xsl:for-each select="tokenize($GLOBAL-REQUEST-POLICY-NAMES, ',')">
                <xsl:element name="Step">
                    <xsl:element name ="Name">
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
<!--
	<xsl:template match="/ProxyEndpoint/HTTPProxyConnection/BasePath">
		<xsl:element name="BasePath">
			<xsl:value-of select="$BASE_PATH"/>
        </xsl:element>
    </xsl:template>
-->			
</xsl:stylesheet>