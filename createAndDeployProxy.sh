#!/bin/sh

#SET VARIABLES:
PROXY_NAME=naics-rest
OAS_DEFINITION=sample-naics-oas-3.0.yaml
APIGEE_ENV=test
BASE_PATH=/rest/naics
APIGEE_USER=username
APIGEE_PASSWORD=password
APIGEE_ORG=org

#Clean up prior runs of the same proxy name
rm -rf $PROXY_NAME

function join_by { local IFS="$1"; shift; echo "$*"; }

openapi2apigee generateApi $PROXY_NAME -s $OAS_DEFINITION -d .
rm -f $PROXY_NAME/apiproxy.zip

xslt3 -xsl:AddPolicies.xsl -s:$PROXY_NAME/apiproxy/proxies/default.xml -o:temp.xml -t GLOBAL-REQUEST-POLICY-NAMES=SA-Spike-Arrest,VK-Verify-API-Key,AM-Remove-API-Key,QP-Product-Quota,JT-JSON-Threat-Protection BASE_PATH=$BASE_PATH
rm $PROXY_NAME/apiproxy/proxies/default.xml
mv temp.xml $PROXY_NAME/apiproxy/proxies/default.xml

cp global-policies/*.xml $PROXY_NAME/apiproxy/policies

apigeetool deployproxy -u $APIGEE_USER -p $APIGEE_PASSWORD -o $APIGEE_ORG -e $APIGEE_ENV -n $PROXY_NAME -d $PROXY_NAME
